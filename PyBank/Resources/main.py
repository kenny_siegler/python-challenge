import csv
import os
# Read CSV
file = os.path.join('../Resources/budget_data.csv')
analysis = os.path.join('/Users/kennysiegler/Desktop/WashU Data Analytics Bootcamp/python-challenge/PyBank/analysis/analysis.txt')

# Main Function
def main():
    #Set Variables
    months = 0
    net = 0
    changes = []
    largest_increase = 0
    largest_loss = 0
    # Read CSV
    with open(file, 'r') as read_file:
        data = csv.reader(read_file, delimiter=',')
        next(data)
        # For loop
        for row in data:
            # count months
            months = months + 1
            # set change variable and calcualte net
            change = int(row[1])
            net = net + change
            # Save each cahnge to a list and find average change
            changes.append(change)
            average = round(sum(changes)/ len(changes), 2)

            # determing largest gain and loss
            if (change > largest_increase):
                max_row = row[0]
                largest_increase = change
            if (change < largest_loss):
                min_row = row[0]
                largest_loss = change
            
        largest_loss = largest_loss * -1
        ## Run secondary functions
        print_results(months, net, average, max_row ,largest_increase, min_row ,largest_loss)
        write_results(months, net, average, max_row ,largest_increase, min_row ,largest_loss)

# Create print function to store prints away from main function
def print_results(months, net, average, max_row ,largest_increase, min_row ,largest_loss):
    print(f"Total months: {months}")
    print(f"Total change is ${net}")
    print(f"Average change is ${average}")
    print(f"{max_row} had the largest increase of ${largest_increase}")
    print(f"{min_row} had the largest loss of ${largest_loss}")


# Create write function to store document writing away from main function
def write_results(months, net, average, max_row ,largest_increase, min_row ,largest_loss):
    with open(analysis, 'w') as new_file:
        new_file.write("Analysis \n")
        new_file.write("________________________________________________________ \n")
        new_file.write(f"Total months: {months} \n")
        new_file.write(f"Total change is ${net} \n")
        new_file.write(f"Average change is ${average} \n")
        new_file.write(f"{max_row} had the largest increase of ${largest_increase} \n")
        new_file.write(f"{min_row} had the largest loss of ${largest_loss} \n")

main()