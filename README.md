# python-challenge

# Editing this README

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Python Challenge/ Weekly Challenge 3

## Description
This is a small analysis of sample data for both bank transactions and election data





## Usage
I'm really not sure how all of this is supposed to be structured. But I had to move the python script into the same folder as the csv because I've never been able to get the os.path.join method or pathlib's Path() method to work unless the two files are in the same folder and I'm running the script from the folder where both files are located.


## Contributing
I used W3 Schools and pythontutorial.net to help me with parts I wasn't sure about like reading and writing to and from files.

