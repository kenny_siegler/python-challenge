import csv
import os
## Read in CSV File. 
file = os.path.join('../Resources/election_data.csv')
analysis = os.path.join('/Users/kennysiegler/Desktop/WashU Data Analytics Bootcamp/python-challenge/PyPoll/analysis/analysis.txt')
# Main function
def main():
    # SET Variables
    votes = 0
    candidates = []
    count1 = 0
    count2 = 0
    count3 = 0
    counts = []
    # Open CSV
    with open(file, 'r') as read_file:
        reader = csv.reader(read_file, delimiter=',')
        # Skip header
        next(reader)
        #For Loop
        for row in reader:
            votes = votes + 1
            #Adding candidates to list of candidates
            if (row[2] not in candidates):
                candidates.append(row[2])

            # Counting votes for each candidate
            if row[2] == candidates[0]:
                count1 = count1 +1
            elif row[2] == candidates[1]:
                count2 = count2 + 1
            else:
                count3 = count3 + 1
        #Adding candidates votes to a list
        counts.append(count1)
        counts.append(count2)
        counts.append(count3)
        # Checking to see who won
        for i in range(len(counts)):
            if counts[i] == max(counts):
                winner = candidates[i]
        # Run Print results and Write Results functions
        print_results(votes, candidates, counts, winner)
        write_results(votes, candidates, counts, winner)
        

# New function to separate prints from main function
def print_results(votes, candidates, counts, winner):
    
    # Get percentages
    per1 = round(((counts[0] / votes)*100), 2)
    per2 = round(((counts[1] / votes)*100), 2)
    per3 = round(((counts[2] / votes)*100), 2)
    
    # Printing all necessary results
    print(f"Total votes: {votes}")
    
    print(f"{candidates[0]}: {per1}% ({counts[0]})")
    print(f"{candidates[1]}: {per2}% ({counts[1]})")
    print(f"{candidates[2]}: {per3}% ({counts[2]})")
    print(f"{winner} is the winner!")


# Function to separate write functions from main functions
def write_results(votes, candidates, counts, winner):
    # calculate percentages
    per1 = round(((counts[0] / votes)*100), 2)
    per2 = round(((counts[1] / votes)*100), 2)
    per3 = round(((counts[2] / votes)*100), 2)
    #writing all necessary results to a new file
    with open(analysis, 'w') as new_file:
        new_file.write("Analysis \n")
        new_file.write("______________________________ \n\n")
        new_file.write(f"Total votes: {votes} \n")
        new_file.write("______________________________ \n\n")
        new_file.write(f"{candidates[0]}: {per1}% ({counts[0]}) \n")
        new_file.write(f"{candidates[1]}: {per2}% ({counts[1]}) \n")
        new_file.write(f"{candidates[2]}: {per3}% ({counts[2]}) \n")
        new_file.write("______________________________ \n\n")
        new_file.write(f"{winner} is the winner! \n")

# Run main function
main()